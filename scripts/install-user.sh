#!/bin/bash

for k in bash mksh zsh ; do
	ln -sf /shl/bin/env/$k $2"/."$k"rc"
done

chsh -s /bin/zsh -u $1

