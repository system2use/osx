#!/bin/bash

echo "Shelter bootstrap for Mac OS X :"
echo "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="

machinto auto-dmg xcode_5.1.1.dmg                      "Xcode"              "https://www.dropbox.com/s/iq4tctj11lon9nx/xcode_5.1.1.dmg?dl=1"

xcode-select -switch /Applications/Xcode.app
xcodebuild -license

machinto auto-dmg XQuartz-2.7.5.dmg                    "XQuartz"            "https://www.dropbox.com/s/udqv76fcho3x94d/XQuartz-2.7.5.dmg?dl=1"

#machinto auto-dmg mysql-5.6.17-osx10.7-x86.dmg        "MySQL-5.6"          "http://cdn.mysql.com/Downloads/MySQL-5.6/mysql-5.6.17-osx10.7-x86.dmg"

#machinto auto-dmg SourceTree_1.9.2.dmg                "SourceTree"         "http://downloads.atlassian.com/software/sourcetree/SourceTree_1.9.2.dmg"
#machinto auto-pkg GitHub%20for%20Mac%20178.zip        "GitHub"             "https://github-central.s3.amazonaws.com/mac/GitHub%20for%20Mac%20178.zip"

#machinto auto-pkg eclipse-standard-kepler-SR2-macosx-cocoa.tar.gz          "http://mirror.ufs.ac.za/eclipse/technology/epp/downloads/release/kepler/SR2/eclipse-standard-kepler-SR2-macosx-cocoa.tar.gz"

#machinto auto-dmg gimp-2.8.10-dmg-1.dmg               "Gimp"               "http://download.gimp.org/pub/gimp/v2.8/osx/gimp-2.8.10-dmg-1.dmg"
#machinto auto-dmg Inkscape-0.48.2-1-SNOWLEOPARD.dmg   "Inkscape"           "http://downloads.sourceforge.net/inkscape/Inkscape-0.48.2-1-SNOWLEOPARD.dmg"
#machinto auto-pkg blender-2.70a-OSX_10.6-i386.zip                          "http://ftp.halifax.rwth-aachen.de/blender/release/Blender2.70/blender-2.70a-OSX_10.6-i386.zip"

#machinto auto-dmg iGetter2.9.2.dmg                    "iGetter 2.9.2"      "http://www.igetter.net/search/downloads/iGetter2.9.2.dmg"
#machinto auto-dmg DIX1.0Universal.dmg                 "Disk Inventory X"   "http://www.alice-dsl.net/tjark.derlien/DIX1.0Universal.dmg"

#machinto auto-dmg xmind-macosx-3.4.1.201401221918.dmg "XMind 3.4.1"        "http://www.xmind.net/xmind/downloads/xmind-macosx-3.4.1.201401221918.dmg"
#machinto auto-dmg xbmc-13.0-i386.dmg                  "XBMC 13.0"          "http://mirrors.xbmc.org/releases/osx/i386/xbmc-13.0-i386.dmg"

ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"

brew update

brew install $BREW_PKGs

brew install miredo
brew install mongodb

#ln -sf $RONIN_ROOT/boot/cron /etc/cron.d/shelter
#ln -sf $RONIN_ROOT/boot/motd /etc/motd

