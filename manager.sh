dmg_install () {
}

pkg_install () {
	sudo installer -package $1 -target "/Volumes/Macintosh HD"
}

machinto () {
	case $1
		auto-dmg)
			machinto download "$3"
			
			cd $RONIN_ROOT/var/packages
			
			machinto dmg "$1" "$2"
			;;
		auto-pkg)
			machinto download "$2"
			
			cd $RONIN_ROOT/var/packages
			
			machinto pkg "$1"
			;;
		download)
			cd $RONIN_ROOT/var/packages
			
			http --download $2
			;;
		dmg)
			if [[ -f "$2" ]] ; then
				sudo hdiutil mount $1

				if [[ ! -d "/Applications/$3.app" ]] ; then
					if [[ -d "/Volumes/*$3*/$3.app" ]] ; then
						sudo cp -R "/Volumes/*$3*/$3.app" /Applications/
					fi

					if [[ -d "/Volumes/*$3*/$3" ]] ; then
						sudo cp -R "/Volumes/*$3*/$3" "/Applications/$3.app"
					fi
				fi

				sudo hdiutil unmount "/Volumes/$3"
			fi
			;;
		pkg)
			sudo installer -package $2 -target "/Volumes/Macintosh HD"
			;;
	esac
}

export BREW_PKGs="htop wget "`echo ext{2,4}fuse`

#export APT_PKGs="aptitude mksh zsh zsh-lovers"
#export APT_PKGs=$APT_PKGs" htop fping nmap traceroute"
#export APT_PKGs=$APT_PKGs" wvdial hostapd tor miredo" #i2p
#export APT_PKGs=$APT_PKGs" git-all etckeeper"
#export APT_PKGs=$APT_PKGs" nginx redis-server rabbitmq-server mysql-server"

