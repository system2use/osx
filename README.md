Preparing target host :
===========================

First, we start by installing the following packages under MacOS X :

* [Xcode](https://www.dropbox.com/s/iq4tctj11lon9nx/xcode_5.1.1.dmg?dl=1)
* [XQuartz](https://www.dropbox.com/s/udqv76fcho3x94d/XQuartz-2.7.5.dmg?dl=1)

Then we proceed by installing [HomeBrew](http://brew.sh/) and it's dependencies.

```bash
sudo brew update

sudo brew install ext{2,4}fuse htop wget
sudo brew install nodejs
sudo brew install miredo
sudo brew install mongodb
```
